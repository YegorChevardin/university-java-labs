package ua.khpi.oop.chevardin05;

import ua.khpi.oop.chevardin05.containers.InformationContainer;
import ua.khpi.oop.chevardin05.util.ReplaceText;

import java.util.Scanner;

/**
 * 5 laboratory work
 * Make a container that will keep an elements from the task 4 (3 laboratory work)
 *
 * @version 1 29 Oct 2022
 * @author Yegor Chevardin
 */

public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please, type here how much words do you want to change: ");
		final int count = inputIntNumber(scanner);

		InformationContainer container = new InformationContainer();

		for (int i = 0; i < count; i++) {
			System.out.println("Please, type here your word (in line): ");
			String text = scanner.nextLine();
			System.out.println("Please, type here character number to replace (in line): ");
			int number = inputIntNumber(scanner);
			System.out.println("Please, type here character to replace (in line, if string inputted - first character of that string will be selected): ");
			String characterString = scanner.nextLine();
			char character = characterString.charAt(0);
			container.add(ReplaceText.replaceString(text, number, character));
		}

		System.out.println("All words in this program: " + container + ". Size of the container is: " + container.size());
	}

	/**
	 * Method that takes integer from input
	 * @param scanner Scenner that will be used while taking a number from input
	 * @return integer that was typed by user
	 */
	private static int inputIntNumber(Scanner scanner) {
		int number = 0;
		try {
			number = Integer.parseInt(scanner.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("Wrong number typed, 0 was returned!");
		}
		return number;
	}
}
