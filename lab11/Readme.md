# lab 11 screenshots
No auto mode with valid numbers:
![1st screen](./1.png)
No auto mode with invalid numbers:
![2nd screen](./2.png)
Auto mode:
![3rd screen](./3.png)