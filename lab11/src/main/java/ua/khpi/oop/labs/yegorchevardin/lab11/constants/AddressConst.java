package ua.khpi.oop.labs.yegorchevardin.lab11.constants;

import java.util.Arrays;

/**
 * Represents constants for address book
 */
public enum AddressConst {
    SOME("96 Parker Rd."),
    SOME1("Hanover, PA 17331\n"),
    SOME2("7669 N. Lake Ave.");
    private final String value;
    private AddressConst(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static String[] getStringValues() {
        return Arrays.stream(values()).map(AddressConst::getValue).toArray(String[]::new);
    }
}
