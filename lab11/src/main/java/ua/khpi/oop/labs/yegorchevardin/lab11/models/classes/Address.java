package ua.khpi.oop.labs.yegorchevardin.lab11.models.classes;

import ua.khpi.oop.labs.yegorchevardin.lab11.exceptions.DataInvalidException;

import java.io.Serializable;
import java.util.regex.Pattern;

/**
 * Address represents address as DTO object
 */
public class Address implements Serializable {
    private static final String PATTERN = "^st\\..*";
    private final String value;

    private Address(String value) {
        this.value = value;
    }

    /**
     * Method that creates address object if value for address is valid
     * @param value Value for address
     * @return Address object with valid address value
     * @throws DataInvalidException in case if passed address value is not correct
     */
    public static Address createAddress(String value) {
        if(!validateAddress(value)) {
            throw new DataInvalidException(
                    "Passed value for address is not valid!" +
                            "Address should include Addr# Street " +
                            "Name, City, State ZIP code"
            );
        }
        return new Address(value);
    }

    /**
     * Method that validate address
     */
    public static boolean validateAddress(String value) {
        return Pattern.compile(PATTERN).matcher(value).matches();
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "[" + value + "]";
    }
}
