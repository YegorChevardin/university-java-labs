package ua.khpi.oop.labs.yegorchevardin.lab11;

import ua.khpi.oop.labs.yegorchevardin.lab11.exceptions.DataInvalidException;
import ua.khpi.oop.labs.yegorchevardin.lab11.models.classes.Address;
import ua.khpi.oop.labs.yegorchevardin.lab11.models.classes.Note;
import ua.khpi.oop.labs.yegorchevardin.lab11.models.classes.PhoneNumber;
import ua.khpi.oop.labs.yegorchevardin.lab11.models.classes.User;
import ua.khpi.oop.labs.yegorchevardin.lab11.models.containers.AddressBook;
import ua.khpi.oop.labs.yegorchevardin.lab11.models.containers.UserStorage;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.sql.Date;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

/**
 * Class that represents prgoram
 */
public class Program {
    private static Program instance = null;
    private final boolean autoMode;

    private Program(boolean autoMode) {
        this.autoMode = autoMode;
    }

    /**
     * Gets only one instance of the program
     */
    public static Program getInstance(boolean autoMode) {
        if (instance == null) {
            instance = new Program(autoMode);
        }
        return instance;
    }

    /**
     * Starts the program
     */
    public void start() {
        AddressBook addressBook = new AddressBook();
        Scanner scanner = null;

        if (autoMode) {
            try {
                scanner = new Scanner(Objects
                        .requireNonNull(this.getClass()
                                .getClassLoader()
                                .getResourceAsStream("inputFile.txt")
                        ));
            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.exit(0);
            }
        } else {
            scanner = new Scanner(System.in);
        }

        System.out.println("Type here amount of notes you want to create");
        int times = 1;
        try {
            times = getIntegerFromInput(scanner, autoMode);
        } catch (IllegalArgumentException e) {
            System.out.println("Bounds must be positive number!");
            System.exit(0);
        }
        Note[] seedingNotes = new Note[times];
        for (int i = 0; i < seedingNotes.length; i++) {
            System.out.println("Please, type here address to add to element number: " + (i + 1));
            String address = scanner.nextLine();
            System.out.println("Please, type here phone number for the element number: " + (i + 1));
            String phoneNumber = scanner.nextLine();
            try {
                seedingNotes[i] = new Note(
                        PhoneNumber.createPhoneNumber(phoneNumber),
                        Address.createAddress(address)
                );
            } catch (DataInvalidException e) {
                System.out.println("Data you input is invalid!");
                System.out.println(e.getMessage());
                System.exit(0);
            }
        }
        run(scanner, addressBook, seedingNotes);
    }

    private <T> void run(Scanner scanner, UserStorage<T> userStorage, T[] elements) {
        System.out.println("Please, register new Account: ");
        User user = register(scanner, autoMode);
        userStorage.setUser(user);
        System.out.println("Seeding passed elements to your user storage...");
        userStorage.addAll(Arrays.asList(elements));
        printContainer(userStorage);
        System.out.println("Removing some elements...");
        userStorage.remove(elements[new Random().nextInt(elements.length)]);
        System.out.println("After removing:");
        printContainer(userStorage);
    }

    private static User register(Scanner scanner, boolean autoMode) {
        String name, fatherName, secondName;
        int day, month, year;

        System.out.println("Please type your name: ");
        name = scanner.nextLine();
        System.out.println("Please type your second name: ");
        secondName = scanner.nextLine();
        System.out.println("Please type your father name: ");
        fatherName = scanner.nextLine();
        System.out.println("Please type a day of birth: ");
        day = getIntegerFromInput(scanner, autoMode);
        System.out.println("Please type a month of birth: ");
        month = getIntegerFromInput(scanner, autoMode);
        System.out.println("Please type a year of birth: ");
        year = getIntegerFromInput(scanner, autoMode);
        Date birthDay = new Date(year - 1900, month - 1, day);

        return new User(name, secondName, fatherName, birthDay);
    }

    private <T> void printContainer(UserStorage<T> container) {
        System.out.println("Container data: ");
        System.out.println("Length: " + container.size());
        System.out.println("Elements: " + container);
    }

    private static int getIntegerFromInput(Scanner scanner, boolean autoMode) {
        try {
            return Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You typed wrong value, please, try again: ");
            if (autoMode) {
                System.exit(0);
            }
        }
        return getIntegerFromInput(scanner, autoMode);
    }
}
