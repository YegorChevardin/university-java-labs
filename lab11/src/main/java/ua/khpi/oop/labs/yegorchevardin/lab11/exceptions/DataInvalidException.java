package ua.khpi.oop.labs.yegorchevardin.lab11.exceptions;

/**
 * Excpetion for case if some data did not passed the validation
 */
public class DataInvalidException extends RuntimeException {
    public DataInvalidException(String message) {
        super(message);
    }
}
