package ua.khpi.oop.labs.yegorchevardin.lab11;

/**
 * 11 laboratory work
 * Add validation through regular expressions and reading through file in automate system run to lab10.
 *
 * @version 28 Mar 2023
 * @author Yegor Chevardin
 */
public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		boolean autoMode = false;
		for (String arg : args) {
			if (arg.equals("-a") || arg.equals("-auto")) {
				autoMode = true;
				break;
			}
		}
		Program.getInstance(autoMode).start();
	}
}
