package ua.khpi.oop.chevardin03;

import java.util.Scanner;

/**
 * 3 laboratory work
 * Task 4. Enter text. Replace specific character from text without using replaceAll() and other methods of this kind
 *
 * @version 1 26 Sep 2022
 * @author Yegor Chevardin
 */

public class Main {
	/**
	 * The point of enter
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please, type here your text (in line): ");
		String text = scanner.nextLine();
		System.out.println("Please, type here number of character to replace (in line): ");
		int number = 0;
		try {
			number = Integer.parseInt(scanner.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("Wrong number typed, 0 selected as a replace number!");
			number = 0;
		}
		System.out.println("Please, type here character to replace (in line, if string inputted - first character of that string will be selected): ");
		String characterString = scanner.nextLine();
		char character = characterString.charAt(0);

		ReplaceText.replaceString(text, number, character);
	}
}
