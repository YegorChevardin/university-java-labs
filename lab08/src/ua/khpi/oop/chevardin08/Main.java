package ua.khpi.oop.chevardin08;

/**
 * 8 laboratory work
 * Save and read a container from object that was created in lab07 without serialization, make dialog with user
 *
 * @version 1 8 Nov 2022
 * @author Yegor Chevardin
 */
public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		AddressBookProgram program = AddressBookProgram.getInstance();
		program.start();
		System.out.println(program.getAddressBook());
	}
}
