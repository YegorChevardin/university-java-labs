package ua.khpi.oop.chevardin08.utils;

import java.util.Scanner;

public class ApplicationCLIInput {
    private ApplicationCLIInput() {}

    /**
     * Method that takes integer from input
     * @param scanner Scenner that will be used while taking a number from input
     * @return integer that was typed by user
     */
    public static int inputIntNumber(Scanner scanner) {
        int number = 0;
        try {
            number = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Passed wrong number, o selected");
        }
        return number;
    }

    /**
     * Method that asks y to accept or n to not accept the action
     * @param scanner Scanner to ask user for input
     * @return true if action is accepted, false otherwise
     */
    public static boolean getAccept(Scanner scanner, String message) {
        System.out.println(message);
        String answer = scanner.nextLine();

        if (answer.equalsIgnoreCase("y")) {
            return true;
        } else if (answer.equalsIgnoreCase("n")) {
            return false;
        } else {
            System.out.println("Wrong string passed, false selected.");
            return false;
        }
    }
}
