package ua.khpi.oop.chevardin02;

import java.util.Random;

/**
 * Laboratory work number 2
 * Task number 9. Count the number of digits that are represented by Latin letters in the hexadecimal notation of a 10-digit integer.
 *
 * @version 1.0 23 Sep 2022
 * @author Yegor Chevardin
 */

public class Main {
	/**
	 * method for converting long number value (10 digits) to 16 bit
	 * @param number number, with size of 10 digits
	 */
	public static int convert(long number) {
		if (String.valueOf(number).length() != 10) throw new IllegalArgumentException("Number length not equals 10 digits!");
		int counter = 0;
		while(number >= 16) {
			long remainder = number % 16;
			number /= 16;
			if(remainder >= 10) {
				counter++;
			}
		}

		return counter;
	}
	
	/**
	 * method for printing table
	 * @param number long value (10 digits size)
	 */
	public static void makeTable(long number) {
		int counter = convert(number);

		System.out.println("   " + number + "   " + counter * 2);
	}
	
	/**
	 * Point of enterence to the program
	 */
	public static void main(String[] args) {
		System.out.println("   Number      Size   ");
		long randomNumber = 0;
		Random random = new Random();
		for (int i = 0; i < 10; i++) {
			randomNumber = random.nextLong(10000000000L - 1000000000L) + 1000000000L;;
			makeTable(randomNumber);
		}		
	}
}
