package ua.khpi.oop.labs.yegorchevardin.lab15.models.classes;

import java.io.Serializable;

/**
 * User representation as DTO object
 */
public class User implements Serializable {
    private String name;

    public User(String name) {
        this.name = name;
    }

    public User() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "[" + name + "]";
    }
}
