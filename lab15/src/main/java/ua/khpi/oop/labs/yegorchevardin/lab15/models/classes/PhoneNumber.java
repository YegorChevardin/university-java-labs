package ua.khpi.oop.labs.yegorchevardin.lab15.models.classes;

import ua.khpi.oop.labs.yegorchevardin.lab15.exceptions.DataInvalidException;
import java.io.Serializable;
import java.util.regex.Pattern;

/**
 * Phone number represent serializable phone number as DTO object
 */
public class PhoneNumber implements Serializable {
    private static final String PHONE_REGEX = "^[\\+]?[(]?[0-9]{3}[)]?[-\\s" +
            "\\.]?[0-9]{3}[-\\s\\.]?[0-9]{4,6}$";
    private final String value;

    private PhoneNumber(String value) {
        this.value = value;
    }

    public static PhoneNumber createPhoneNumber(String value) {
        if(!validatePhoneNUmber(value)) {
            throw new DataInvalidException(
                    "Inserted values is not phone number!"
            );
        }
        return new PhoneNumber(value);
    }

    /**
     * Method that validates string whether it is phone number or not
     * @param phoneNumber string to be validated
     * @return valid or not (true or false)
     */
    public static boolean validatePhoneNUmber(String phoneNumber) {
        return Pattern.compile(PHONE_REGEX).matcher(phoneNumber).matches();
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "[" + value + "]";
    }
}
