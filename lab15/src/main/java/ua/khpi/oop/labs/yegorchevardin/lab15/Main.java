package ua.khpi.oop.labs.yegorchevardin.lab15;

/**
 * 15 laboratory work
 * remake 14 laboratory work to 15th
 * @version 18 Apr 2023
 * @author Yegor Chevardin
 */
public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		Program.getInstance().start();
	}
}
