package ua.khpi.oop.chevardin07.models.classes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumber {
    private static final String PHONE_REGEX = "^[\\+]?[(]?[0-9]{3}[)]?[-\\s\\.]?[0-9]{3}[-\\s\\.]?[0-9]{4,6}$";
    private final String value;

    private PhoneNumber(String value) {
        this.value = value;
    }

    /**
     * Method that validates string whether it is phone number or not
     * @param phoneNumber string to be validated
     * @return valid or not (true or false)
     */
    public static boolean validatePhoneNUmber(String phoneNumber) {
        Pattern pattern = Pattern.compile(PHONE_REGEX);
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.matches();
    }

    /**
     * Method that creates a PhoneNumber object, with valid value of phone number
     * @param phoneNumber phone number String
     * @return valid object with phone number
     * @throws IllegalArgumentException if the passed string is not phone number
     */
    public static PhoneNumber createPhoneNumber(String phoneNumber) {
        if (!validatePhoneNUmber(phoneNumber)) {
            throw new IllegalArgumentException("Inserted values is not phone number!");
        }
        return new PhoneNumber(phoneNumber);
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
