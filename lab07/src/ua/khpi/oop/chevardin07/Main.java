package ua.khpi.oop.chevardin07;

/**
 * 7 laboratory work
 * Create an object of task and show how to work with container of those objects and store cyrillic words (Number 3: Address Book)
 *
 * @version 1 5 Nov 2022
 * @author Yegor Chevardin
 */
public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		AddressBookProgram program = AddressBookProgram.getInstance();
		program.start();
		System.out.println(program.getAddressBook());
	}
}
