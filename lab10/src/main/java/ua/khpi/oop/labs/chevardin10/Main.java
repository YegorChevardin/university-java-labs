package ua.khpi.oop.labs.chevardin10;

/**
 * 10 laboratory work
 * Create a generic methods and autoplay to lab09.
 *
 * @version 11 Mar 2023
 * @author Yegor Chevardin
 */
public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		boolean autoMode = false;
		for (String arg : args) {
			if (arg.equals("-a") || arg.equals("-auto")) {
				autoMode = true;
				break;
			}
		}
		Program.getInstance(autoMode).start();
	}
}
