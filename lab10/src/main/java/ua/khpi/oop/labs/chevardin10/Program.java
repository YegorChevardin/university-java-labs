package ua.khpi.oop.labs.chevardin10;

import ua.khpi.oop.labs.chevardin10.constants.AddressConst;
import ua.khpi.oop.labs.chevardin10.constants.PhoneNumberConst;
import ua.khpi.oop.labs.chevardin10.models.classes.Address;
import ua.khpi.oop.labs.chevardin10.models.classes.Note;
import ua.khpi.oop.labs.chevardin10.models.classes.PhoneNumber;
import ua.khpi.oop.labs.chevardin10.models.classes.User;
import ua.khpi.oop.labs.chevardin10.models.containers.AddressBook;
import ua.khpi.oop.labs.chevardin10.models.containers.UserStorage;
import java.sql.Date;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Class that represents prgoram
 */
public class Program {
    private static final int ARRAY_SIZE = 2;
    private static Program instance = null;
    private boolean autoMode = false;

    private Program(boolean autoMode) {
        this.autoMode = autoMode;
    }

    /**
     * Gets only one instance of the program
     */
    public static Program getInstance(boolean autoMode) {
        if (instance == null) {
            instance = new Program(autoMode);
        }
        return instance;
    }

    /**
     * Starts the program
     */
    public void start() {
        AddressBook addressBook = new AddressBook();
        if (autoMode) {
            Note[] notes = new Note[ARRAY_SIZE];
            for (int i = 0; i < notes.length; i++) {
                notes[i] = new Note(
                        new PhoneNumber(PhoneNumberConst.getStringValues()[
                                new Random().nextInt(PhoneNumberConst.getStringValues().length)
                                ]),
                        new Address(AddressConst.getStringValues()[
                                new Random().nextInt(AddressConst.getStringValues().length)
                                ])
                );
            }
            runAuto(addressBook, notes);
        } else {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Type here amount of notes you want to create");
            int times = getIntegerFromInput(scanner);
            Note[] seedingNotes = new Note[times];
            for (int i = 0; i < seedingNotes.length; i++) {
                System.out.println("Please, type here address to add to element number: " + (i + 1));
                String address = scanner.nextLine();
                System.out.println("Please, type here phone number for the element number: " + (i + 1));
                String phoneNumber = scanner.nextLine();
                seedingNotes[i] = new Note(
                        new PhoneNumber(phoneNumber),
                        new Address(address)
                );
            }
            run(scanner, addressBook, seedingNotes);
        }
    }

    private <T> void run(Scanner scanner, UserStorage<T> userStorage, T[] elements) {
        System.out.println("Please, register new Account: ");
        User user = register(scanner);
        userStorage.setUser(user);
        System.out.println("Seeding passed elements to your user storage...");
        userStorage.addAll(Arrays.asList(elements));
        printContainer(userStorage);
        System.out.println("Removing some elements...");
        userStorage.remove(elements[new Random().nextInt(elements.length)]);
        System.out.println("After removing:");
        printContainer(userStorage);
    }

    private <T> void runAuto(UserStorage<T> userStorage, T[] objectsToAdd) {
        printContainer(userStorage);
        System.out.println("Adding new elements to it...");
        userStorage.addAll(Arrays.asList(objectsToAdd));
        System.out.println("After adding elements: ");
        printContainer(userStorage);
        System.out.println("Deleting elements...");
        userStorage.remove(objectsToAdd[new Random().nextInt(objectsToAdd.length)]);
        System.out.println("After deleting elements...");
        printContainer(userStorage);
        System.out.println("Program is finished.");
    }

    private static User register(Scanner scanner) {
        String name, fatherName, secondName;
        int day, month, year;

        System.out.println("Please type your name: ");
        name = scanner.nextLine();
        System.out.println("Please type your second name: ");
        secondName = scanner.nextLine();
        System.out.println("Please type your father name: ");
        fatherName = scanner.nextLine();
        System.out.println("Please type a day of birth: ");
        day = getIntegerFromInput(scanner);
        System.out.println("Please type a month of birth: ");
        month = getIntegerFromInput(scanner);
        System.out.println("Please type a year of birth: ");
        year = getIntegerFromInput(scanner);
        Date birthDay = new Date(year - 1900, month - 1, day);

        return new User(name, secondName, fatherName, birthDay);
    }

    private <T> void printContainer(UserStorage<T> container) {
        System.out.println("Container data: ");
        System.out.println("Length: " + container.size());
        System.out.println("Elements: " + container);
    }

    private static int getIntegerFromInput(Scanner scanner) {
        try {
            return Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You typed wrong value, please, try again: ");
            return getIntegerFromInput(scanner);
        }
    }
}
