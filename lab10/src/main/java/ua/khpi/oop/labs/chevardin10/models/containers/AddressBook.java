package ua.khpi.oop.labs.chevardin10.models.containers;


import ua.khpi.oop.labs.chevardin10.models.classes.Note;
import ua.khpi.oop.labs.chevardin10.models.classes.User;

/**
 * Address book that represents container and extends from UserStorage
 */
public class AddressBook extends UserStorage<Note> {
    public AddressBook(User user) {
        super(user);
    }

    public AddressBook() {}
}
