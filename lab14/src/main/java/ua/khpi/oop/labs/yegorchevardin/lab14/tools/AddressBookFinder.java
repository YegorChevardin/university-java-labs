package ua.khpi.oop.labs.yegorchevardin.lab14.tools;

import ua.khpi.oop.labs.yegorchevardin.lab14.models.classes.Note;
import ua.khpi.oop.labs.yegorchevardin.lab14.models.containers.AddressBook;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Finder of information from Address book
 * @author Yehor Chevardin
 * @version 1.0.0
 */
public class AddressBookFinder {
    private static final String PHONE_BEGIN = "^\\+380?.*";
    private static final String[] KYIVSTAR_NUMBERS_STARTINGS = {"39", "67", "98"};
    private static final String[] LIFESELL_NUMBER_STARTINGS = {"50", "66", "95", "99"};
    private static final String CITY_KHARKIV_PATTERN = "st\\..* Kharkiv";


    private AddressBookFinder() {}

    /**
     * Method for getting all notes that have Kyivstar phone number in Kharkiv
     */
    public static List<Note> findAllNotesByKyivstarNumbersInKharkiv(
            AddressBook addressBook
    ) {
        return findAllNotesByAnyPhoneCompanyInKharkiv(
                addressBook,
                KYIVSTAR_NUMBERS_STARTINGS
        );
    }

    private static List<Note> findAllNotesByAnyPhoneCompanyInKharkiv(
            AddressBook addressBook,
            String[] phoneNumberCompany
    ) {
        List<Note> notes = new ArrayList<>();
        addressBook.forEach(
                (currentNote) -> {
                    if(Pattern.compile(CITY_KHARKIV_PATTERN)
                            .matcher(currentNote.getAddress().getValue()).matches()
                    ) {
                        String currentPhoneNumber = currentNote.getPhoneNumber().getValue();
                        Pattern pattern;

                        for (String phoneNumberStarting : phoneNumberCompany) {
                            pattern = Pattern.compile(PHONE_BEGIN.replace("?", phoneNumberStarting));
                            if (pattern.matcher(currentPhoneNumber).matches()) {
                                notes.add(currentNote);
                                break;
                            }
                        }

                    }
                }
        );
        return notes;
    }

    /**
     * Method for getting all notes that have Lifesell phone number in Kharkiv
     */
    public static List<Note> findAllNotesByLifesellNumberInKharkiv(
            AddressBook addressBook
    ) {
        return findAllNotesByAnyPhoneCompanyInKharkiv(
                addressBook,
                LIFESELL_NUMBER_STARTINGS
        );
    }
}
