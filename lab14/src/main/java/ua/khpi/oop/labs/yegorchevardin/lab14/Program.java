package ua.khpi.oop.labs.yegorchevardin.lab14;

import tech.tablesaw.api.FloatColumn;
import tech.tablesaw.api.StringColumn;
import tech.tablesaw.api.Table;
import ua.khpi.oop.labs.yegorchevardin.lab14.exceptions.DataInvalidException;
import ua.khpi.oop.labs.yegorchevardin.lab14.models.classes.Address;
import ua.khpi.oop.labs.yegorchevardin.lab14.models.classes.Note;
import ua.khpi.oop.labs.yegorchevardin.lab14.models.classes.PhoneNumber;
import ua.khpi.oop.labs.yegorchevardin.lab14.models.classes.User;
import ua.khpi.oop.labs.yegorchevardin.lab14.models.containers.AddressBook;
import ua.khpi.oop.labs.yegorchevardin.lab14.models.containers.UserStorage;
import ua.khpi.oop.labs.yegorchevardin.lab14.tools.AddressBookFinder;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Class that represents program
 */
public class Program {
    private static final int POOL_THREAD = 5;
    private static Program instance = null;

    private Program() {}

    /**
     * Gets only one instance of the program
     */
    public static Program getInstance() {
        if (instance == null) {
            instance = new Program();
        }
        return instance;
    }

    /**
     * Starts the program
     */
    public void start() {
        AddressBook addressBook = new AddressBook();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Type here amount of notes you want to create");
        int times = 1;
        try {
            times = getIntegerFromInput(scanner);
        } catch (IllegalArgumentException e) {
            System.out.println("Bounds must be positive number!");
            System.exit(0);
        }
        Note[] seedingNotes = new Note[times];
        for (int i = 0; i < seedingNotes.length; i++) {
            System.out.println("Please, type here address to add to element number: " + (i + 1));
            String address = scanner.nextLine();
            System.out.println("Please, type here phone number for the element number: " + (i + 1));
            String phoneNumber = scanner.nextLine();
            try {
                seedingNotes[i] = new Note(
                        PhoneNumber.createPhoneNumber(phoneNumber),
                        Address.createAddress(address)
                );
            } catch (DataInvalidException e) {
                System.out.println("Data you input is invalid!");
                System.out.println(e.getMessage());
                System.exit(0);
            }
        }
        run(scanner, addressBook, seedingNotes);
    }

    private <T> void run(Scanner scanner, UserStorage<T> userStorage, T[] elements) {
        try {
            System.out.println("Please, register new Account: ");
            User user = register(scanner);
            userStorage.setUser(user);
            System.out.println("Seeding passed elements to your user storage...");
            userStorage.addAll(Arrays.asList(elements));
            printContainer(userStorage);

            System.out.println("---------Starting multithreading part---------");
            startMultiThreadingTask((AddressBook) userStorage);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
    }

    private static void startMultiThreadingTask(AddressBook addressBook) {
        AtomicReference<Float> result = new AtomicReference<>(0.0F);
        AtomicReference<Float> synchronizedResult = new AtomicReference<>(0.0F);
        Table results = Table.create("Results", List.of(StringColumn.create("Type name"), FloatColumn.create("Time")));
        results.stringColumn("Type name").append("Synchronized");
        results.stringColumn("Type name").append("Non synchronized");
        results.stringColumn("Type name").append("Result");
        Runnable task = () -> {
            result.updateAndGet(v -> {
                try {
                    return v + searchInAddressBook(addressBook);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
        };
        Runnable taskSynchronized = () -> {
            synchronizedResult.updateAndGet(v -> {
                try {
                    return v + searchAddressBookSynchronized(addressBook);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
        };

        ExecutorService executorService = Executors.newFixedThreadPool(POOL_THREAD);

        // Starting synchronized block
        loopTaskForAllThreads(executorService, taskSynchronized);
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }

        // Starting non synchronized block
        executorService = Executors.newFixedThreadPool(POOL_THREAD);
        loopTaskForAllThreads(executorService, task);
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }

        results.floatColumn("Time").append(result.get());
        results.floatColumn("Time").append(synchronizedResult.get());
        results.floatColumn("Time").append(synchronizedResult.get() - result.get());

        System.out.println(results.print());
    }

    private static void loopTaskForAllThreads(ExecutorService executorService, Runnable runnable) {
        for (int i = 0; i < POOL_THREAD; i++) {
            executorService.submit(runnable);
        }
    }

    private synchronized static float searchAddressBookSynchronized(AddressBook addressBook)
            throws InterruptedException {
        return searchInAddressBook(addressBook);
    }

    private static float searchInAddressBook(AddressBook addressBook) throws InterruptedException {
        Long start = System.currentTimeMillis();
        System.out.println(
                Thread.currentThread().getId() + " => " + Thread.currentThread().getName() + " started"
        );
        System.out.println(" finding all phone numbers in Kharkiv with Lifecell and Vodafone");
        System.out.println(
                System.lineSeparator() +
                        AddressBookFinder
                                .findAllNotesByKyivstarNumbersInKharkiv(addressBook) + System.lineSeparator() +
                        AddressBookFinder
                                .findAllNotesByLifesellNumberInKharkiv(addressBook) + System.lineSeparator() +
                        Thread.currentThread().getId() + " => " + Thread.currentThread().getName()
        );
        Thread.sleep(30);

        Long finish = System.currentTimeMillis();

        return finish - start;
    }

    private static User register(Scanner scanner) {
        System.out.println("Please type your name: ");
        String name = scanner.nextLine();
        return new User(name);
    }

    private <T> void printContainer(UserStorage<T> container) {
        System.out.println("Container data: ");
        System.out.println("Length: " + container.size());
        System.out.println("Elements: " + container);
    }

    private static int getIntegerFromInput(Scanner scanner) {
        try {
            return Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You typed wrong value, please, try again: ");
            return getIntegerFromInput(scanner);
        }
    }
}
