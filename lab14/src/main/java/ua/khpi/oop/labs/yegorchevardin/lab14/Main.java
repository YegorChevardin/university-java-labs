package ua.khpi.oop.labs.yegorchevardin.lab14;

/**
 * 13 laboratory work
 * Add measuring time sor synchronized and not synchronized way and display it in the table in lab13
 * @version 18 Apr 2023
 * @author Yegor Chevardin
 */
public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		Program.getInstance().start();
	}
}
