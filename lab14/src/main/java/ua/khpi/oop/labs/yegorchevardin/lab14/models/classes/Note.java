package ua.khpi.oop.labs.yegorchevardin.lab14.models.classes;

import ua.khpi.oop.labs.yegorchevardin.lab14.models.classes.abstractions.StoringBox;

/**
 * Note represents storage box in the address book
 */
public class Note extends StoringBox<PhoneNumber, Address> {
    public Note(PhoneNumber phoneNumber, Address address) {
        super(phoneNumber, address);
    }

    public PhoneNumber getPhoneNumber() {
        return getValue1();
    }

    public Address getAddress() {
        return getValue2();
    }
}
