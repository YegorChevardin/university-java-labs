# Executing result
Executing the jar file:
> `java -jar lab13.jar`
> 
result:
```bash
Type here amount of notes you want to create
2
Please, type here address to add to element number: 1
st. Pushka, Kharkiv
Please, type here phone number for the element number: 1
+380999425174
Please, type here address to add to element number: 2
st. Kyiv Kharkiv
Please, type here phone number for the element number: 2
+380974567432
Please, register new Account: 
Please type your name: 
Yehor
Seeding passed elements to your user storage...
Container data: 
Length: 2
Elements: {[[+380999425174], [st. Pushka, Kharkiv]][[+380974567432], [st. Kyiv Kharkiv]]}
---------Starting multithreading part---------
16 => pool-1-thread-4 started
15 => pool-1-thread-3 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
13 => pool-1-thread-1 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
22 => pool-1-thread-10 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
21 => pool-1-thread-9 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
24 => pool-1-thread-12 started
14 => pool-1-thread-2 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
20 => pool-1-thread-8 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
19 => pool-1-thread-7 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
17 => pool-1-thread-5 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
18 => pool-1-thread-6 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
23 => pool-1-thread-11 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
25 => pool-1-thread-13 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
23 => pool-1-thread-11

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
21 => pool-1-thread-9

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
19 => pool-1-thread-7

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
15 => pool-1-thread-3

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
24 => pool-1-thread-12

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
14 => pool-1-thread-2

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
17 => pool-1-thread-5

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
18 => pool-1-thread-6

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
22 => pool-1-thread-10

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
13 => pool-1-thread-1

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
16 => pool-1-thread-4

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
25 => pool-1-thread-13

[]
[[[+380999425174], [st. Pushka, Kharkiv]]]
20 => pool-1-thread-8
23 => pool-1-thread-11 ended:  time: 44
21 => pool-1-thread-9 ended:  time: 45
19 => pool-1-thread-7 ended:  time: 45
15 => pool-1-thread-3 ended:  time: 47
14 => pool-1-thread-2 ended:  time: 48
24 => pool-1-thread-12 ended:  time: 45
16 => pool-1-thread-4 ended:  time: 49
17 => pool-1-thread-5 ended:  time: 48
25 => pool-1-thread-13 ended:  time: 48
18 => pool-1-thread-6 ended:  time: 49
22 => pool-1-thread-10 ended:  time: 49
20 => pool-1-thread-8 ended:  time: 49
13 => pool-1-thread-1 ended:  time: 51

Process finished with exit code 0

```