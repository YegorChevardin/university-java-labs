package ua.khpi.oop.labs.yegorchevardin.lab13;

/**
 * 13 laboratory work
 * Add multithreading and time experement to the lab12
 *
 * @version 11 Apr 2023
 * @author Yegor Chevardin
 */
public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		Program.getInstance().start();
	}
}
