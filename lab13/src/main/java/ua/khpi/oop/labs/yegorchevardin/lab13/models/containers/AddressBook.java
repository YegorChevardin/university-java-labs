package ua.khpi.oop.labs.yegorchevardin.lab13.models.containers;

import ua.khpi.oop.labs.yegorchevardin.lab13.models.classes.Note;
import ua.khpi.oop.labs.yegorchevardin.lab13.models.classes.User;

/**
 * Address book that represents container and extends from UserStorage
 */
public class AddressBook extends UserStorage<Note> {
    public AddressBook(User user) {
        super(user);
    }

    public AddressBook() {}
}
