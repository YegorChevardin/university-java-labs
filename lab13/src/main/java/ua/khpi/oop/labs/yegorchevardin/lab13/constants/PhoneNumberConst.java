package ua.khpi.oop.labs.yegorchevardin.lab13.constants;

import java.util.Arrays;

/**
 * Represents constants for phone numbers
 */
public enum PhoneNumberConst {
    SOME("+380999425174"),
    SOME1("+48574350682");
    private final String value;

    private PhoneNumberConst(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static String[] getStringValues() {
        return Arrays.stream(values()).map(PhoneNumberConst::getValue).toArray(String[]::new);
    }
}
