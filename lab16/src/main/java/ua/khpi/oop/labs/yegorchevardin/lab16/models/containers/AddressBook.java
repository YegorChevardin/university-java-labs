package ua.khpi.oop.labs.yegorchevardin.lab16.models.containers;

import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.Note;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.User;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Address book that represents container and extends from UserStorage
 */
public class AddressBook extends LinkedList<Note> implements Serializable {
    private User user;

    public AddressBook(User user) {
        this.user = user;
    }

    public AddressBook() {
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return user.toString() + super.toString();
    }
}
