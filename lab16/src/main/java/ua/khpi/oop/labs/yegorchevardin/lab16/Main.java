package ua.khpi.oop.labs.yegorchevardin.lab16;

/**
 * 16 laboratory work
 * Add unit testing to laboratory work number 15
 * @version 22 May 2023
 * @author Yegor Chevardin
 */
public class Main {
	/**
	 * The point of entrance
	 */
	public static void main(String[] args) {
		Program.getInstance().start();
	}
}
