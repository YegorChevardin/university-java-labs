package ua.khpi.oop.labs.yegorchevardin.lab16;

import jakarta.xml.bind.*;
import tech.tablesaw.api.FloatColumn;
import tech.tablesaw.api.StringColumn;
import tech.tablesaw.api.Table;
import ua.khpi.oop.labs.yegorchevardin.lab16.exceptions.DataInvalidException;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.Address;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.Note;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.PhoneNumber;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.User;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.containers.AddressBook;
import ua.khpi.oop.labs.yegorchevardin.lab16.tools.AddressBookFinder;

import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Class that represents program
 */
public class Program {
    private static final int POOL_THREAD = 5;
    private static Program instance = null;

    private Program() {}

    /**
     * Gets only one instance of the program
     */
    public static Program getInstance() {
        if (instance == null) {
            instance = new Program();
        }
        return instance;
    }

    /**
     * Starts the program
     */
    public void start() {
        AddressBook addressBook = new AddressBook();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Type here amount of notes you want to create");
        int times = 1;
        try {
            times = getIntegerFromInput(scanner);
        } catch (IllegalArgumentException e) {
            System.out.println("Bounds must be positive number!");
            System.exit(0);
        }
        Note[] seedingNotes = new Note[times];
        for (int i = 0; i < seedingNotes.length; i++) {
            System.out.println("Please, type here address to add to element number: " + (i + 1));
            String address = scanner.nextLine();
            System.out.println("Please, type here phone number for the element number: " + (i + 1));
            String phoneNumber = scanner.nextLine();
            try {
                seedingNotes[i] = new Note(
                        PhoneNumber.createPhoneNumber(phoneNumber),
                        Address.createAddress(address)
                );
            } catch (DataInvalidException e) {
                System.out.println("Data you input is invalid!");
                System.out.println(e.getMessage());
                System.exit(0);
            }
        }
        run(scanner, addressBook, List.of(seedingNotes));
    }

    private void run(Scanner scanner, AddressBook userStorage, Collection<Note> elements) {
        try {
            System.out.println("Please, register new Account: ");
            User user = register(scanner);
            userStorage.setUser(user);
            System.out.println("Seeding passed elements to your user storage...");
            userStorage.addAll(elements);
            printContainer(userStorage);

            System.out.println("---------Starting multithreading part---------");
            startMultiThreadingTask(userStorage);

            System.out.println("---------Testing serialization part---------");
            serializeWithProtocolAndWithout(userStorage);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
    }

    private static void serializeWithProtocolAndWithout(AddressBook userStorage) {
        serializeWithProtocol(userStorage);
        System.out.println(deserializeWithProtocol());
        serializeWithoutProtocol(userStorage);
        System.out.println(deserializeObjectWithoutProtocol());
    }

    private static void serializeWithoutProtocol(AddressBook addressBook) {
        try {
            JAXBElement<AddressBook> jaxbElement = new JAXBElement<>(new QName("", "AddressBook"), AddressBook.class, addressBook);
            JAXBContext jaxbContext = JAXBContext.newInstance(AddressBook.class);

            //Create Marshaller
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Store XML to File
            File file = new File(Objects.requireNonNull(Program.class.getClassLoader().getResource("/objectXML.xml")).getPath());

            //Writes XML file to file-system
            jaxbMarshaller.marshal(jaxbElement, file);
        } catch (JAXBException e) {
            throw new RuntimeException(e.getMessage());
        }

    }

    private static Object deserializeObjectWithoutProtocol() {
        File xmlFile = new File(Objects.requireNonNull(Program.class.getClassLoader().getResource("/objectXML.xml")).getPath());
        JAXBContext jaxbContext;

        try {
            jaxbContext = JAXBContext.newInstance(AddressBook.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            JAXBElement<AddressBook> jaxbElement = jaxbUnmarshaller
                    .unmarshal(new StreamSource(xmlFile), AddressBook.class);
            return jaxbElement.getValue();
        }
        catch (JAXBException e)
        {
            throw new RuntimeException(e.getMessage());
        }

    }

    private static void serializeWithProtocol(AddressBook addressBook) {
        System.out.println("Storing address book in file...");
        try (FileOutputStream fileOutputStream = new FileOutputStream(
                Objects.requireNonNull(Program.class.getClassLoader().getResource("/object.ser")).getPath()
        );
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(addressBook);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Storing completed successfully...");

    }

    private static Object deserializeWithProtocol() {
        System.out.println("Deserializing the object...");
        Object container;
        try (FileInputStream fileInputStream = new FileInputStream("/object.ser");
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            container = objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Deserializing object completed...");
        return container;
    }

    private static void startMultiThreadingTask(AddressBook addressBook) {
        AtomicReference<Float> result = new AtomicReference<>(0.0F);
        AtomicReference<Float> synchronizedResult = new AtomicReference<>(0.0F);
        Table results = Table.create("Results", List.of(StringColumn.create("Type name"), FloatColumn.create("Time")));
        results.stringColumn("Type name").append("Synchronized");
        results.stringColumn("Type name").append("Non synchronized");
        results.stringColumn("Type name").append("Result");
        Runnable task = () -> {
            result.updateAndGet(v -> {
                try {
                    return v + searchInAddressBook(addressBook);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
        };
        Runnable taskSynchronized = () -> {
            synchronizedResult.updateAndGet(v -> {
                try {
                    return v + searchAddressBookSynchronized(addressBook);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
        };

        ExecutorService executorService = Executors.newFixedThreadPool(POOL_THREAD);

        // Starting synchronized block
        loopTaskForAllThreads(executorService, taskSynchronized);
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }

        // Starting non synchronized block
        executorService = Executors.newFixedThreadPool(POOL_THREAD);
        loopTaskForAllThreads(executorService, task);
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }

        results.floatColumn("Time").append(result.get());
        results.floatColumn("Time").append(synchronizedResult.get());
        results.floatColumn("Time").append(synchronizedResult.get() - result.get());

        System.out.println(results.print());
    }

    private static void loopTaskForAllThreads(ExecutorService executorService, Runnable runnable) {
        for (int i = 0; i < POOL_THREAD; i++) {
            executorService.submit(runnable);
        }
    }

    private synchronized static float searchAddressBookSynchronized(AddressBook addressBook)
            throws InterruptedException {
        return searchInAddressBook(addressBook);
    }

    private static float searchInAddressBook(AddressBook addressBook) throws InterruptedException {
        Long start = System.currentTimeMillis();
        System.out.println(
                Thread.currentThread().getId() + " => " + Thread.currentThread().getName() + " started"
        );
        System.out.println(" finding all phone numbers in Kharkiv with Lifecell and Vodafone");
        System.out.println(
                System.lineSeparator() +
                        AddressBookFinder
                                .findAllNotesByKyivstarNumbersInKharkiv(addressBook) + System.lineSeparator() +
                        AddressBookFinder
                                .findAllNotesByLifesellNumberInKharkiv(addressBook) + System.lineSeparator() +
                        Thread.currentThread().getId() + " => " + Thread.currentThread().getName()
        );
        Thread.sleep(30);

        Long finish = System.currentTimeMillis();

        return finish - start;
    }

    private static User register(Scanner scanner) {
        System.out.println("Please type your name: ");
        String name = scanner.nextLine();
        return new User(name);
    }

    private <T> void printContainer(LinkedList<T> container) {
        System.out.println("Container data: ");
        System.out.println("Length: " + container.size());
        System.out.println("Elements: " + container);
    }

    private static int getIntegerFromInput(Scanner scanner) {
        try {
            return Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You typed wrong value, please, try again: ");
            return getIntegerFromInput(scanner);
        }
    }
}
