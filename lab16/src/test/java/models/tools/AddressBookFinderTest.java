package models.tools;

import org.junit.jupiter.api.Test;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.Address;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.Note;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.PhoneNumber;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.User;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.containers.AddressBook;
import ua.khpi.oop.labs.yegorchevardin.lab16.tools.AddressBookFinder;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for testing searching address book part
 */
public class AddressBookFinderTest {
    private final AddressBook addressBook;
    public AddressBookFinderTest() {
        addressBook = new AddressBook(new User("Test user"));
    }

    /**
     * Method for validating finding a vodafone number
     * one element expected
     */
    @Test
    public void findVodafone_returnOneElement() {
        Note note = new Note(
                PhoneNumber.createPhoneNumber("+380999425174"),
                Address.createAddress("st. Pushkinskaya, Kharkiv")
        );
        addressBook.add(note);

        assertEquals(note,
                AddressBookFinder.findAllNotesByLifesellNumberInKharkiv(addressBook).get(0)
        );
    }

    /**
     * Method for validating finding a vodafone number
     * no elements expected
     */
    @Test
    public void findVodafone_returnEmpty() {
        addressBook.clear();

        assertEquals(
                0,
                AddressBookFinder.findAllNotesByLifesellNumberInKharkiv(addressBook).size()
        );
    }

    /**
     * Method for validating finding a kyivstar number
     * one element expected
     */
    @Test
    public void findKyivstar_returnOneElement() {
        Note note = new Note(
                PhoneNumber.createPhoneNumber("+380679425174"),
                Address.createAddress("st. Pushkinskaya, Kharkiv")
        );
        addressBook.add(note);

        assertEquals(note,
                AddressBookFinder.findAllNotesByKyivstarNumbersInKharkiv(addressBook).get(0)
        );
    }

    /**
     * Method for validating finding a kyivstar number
     * no elements expected
     */
    @Test
    public void findKyivstar_returnEmpty() {
        Note note = new Note(
                PhoneNumber.createPhoneNumber("+380999425174"),
                Address.createAddress("st. Pushkinskaya, Kharkiv")
        );
        addressBook.add(note);

        assertEquals(0,
                AddressBookFinder.findAllNotesByKyivstarNumbersInKharkiv(addressBook).size()
        );
    }
}
