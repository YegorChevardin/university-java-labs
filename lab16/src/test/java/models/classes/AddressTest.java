package models.classes;

import models.classes.abstractions.ModelTestInterface;
import org.junit.jupiter.api.Test;
import ua.khpi.oop.labs.yegorchevardin.lab16.exceptions.DataInvalidException;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.Address;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for testing Address book creation
 */
public class AddressTest implements ModelTestInterface {
    @Override
    @Test
    public void successCreation_returnObject() {
        String correctAddress = "st. Pushkinska, Kharkiv";
        Address address = Address.createAddress(correctAddress);

        assertEquals(correctAddress, address.getValue());
    }

    @Override
    @Test
    public void failCreation_exceptionThrowing() {
        String incorrectAddress = "hfjrjth98345739";

        assertThrows(DataInvalidException.class,
                () -> Address.createAddress(incorrectAddress));
    }
}
