package models.classes;

import models.classes.abstractions.ModelTestInterface;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ua.khpi.oop.labs.yegorchevardin.lab16.exceptions.DataInvalidException;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.PhoneNumber;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for testing the PhoneNumber class object creation
 */
public class PhoneNumberTest implements ModelTestInterface {
    @Override
    @Test
    public void successCreation_returnObject() {
        String successPhoneNumber = "+380999425174";
        PhoneNumber phoneNumber = PhoneNumber
                .createPhoneNumber(successPhoneNumber);

        assertEquals(successPhoneNumber, phoneNumber.getValue());
    }

    @Test
    @Override
    public void failCreation_exceptionThrowing() {
        String incorrectPhoneNumber = "379573hgdkfgd35";

        assertThrows(DataInvalidException.class,
                () -> PhoneNumber.createPhoneNumber(incorrectPhoneNumber));
    }
}
