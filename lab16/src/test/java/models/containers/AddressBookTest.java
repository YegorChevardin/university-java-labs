package models.containers;

import models.containers.abstractions.CreateReadUpdateDeleteContainerTestInterface;
import org.junit.jupiter.api.Test;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.Address;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.Note;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.PhoneNumber;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.classes.User;
import ua.khpi.oop.labs.yegorchevardin.lab16.models.containers.AddressBook;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for testing Address Book container methods
 */
public class AddressBookTest implements CreateReadUpdateDeleteContainerTestInterface {
    private final String username = "TestUser";
    private final String phoneNumberValue = "+380999425174";
    private final String addressBookValue = "st. Pushkinska, Kharkiv";

    @Override
    @Test
    public void createContainer_returnAddressBookContainer() {
        User user = new User(username);
        AddressBook addressBook = new AddressBook(user);

        assertNotNull(addressBook.getUser());
    }

    @Override
    @Test
    public void addElementToContainer_success() {
        User user = new User(username);
        AddressBook addressBook = new AddressBook(user);

        addressBook.add(new Note(
                PhoneNumber.createPhoneNumber(phoneNumberValue),
                Address.createAddress(addressBookValue)
        ));

        assertEquals(addressBook.getUser(), user);
        assertEquals(phoneNumberValue, addressBook.get(0).getPhoneNumber().getValue());
        assertEquals(addressBookValue, addressBook.get(0).getAddress().getValue());
    }

    @Override
    @Test
    public void removeElementFromContainer_success() {
        User user = new User(username);
        AddressBook addressBook = new AddressBook(user);

        addressBook.add(new Note(
                PhoneNumber.createPhoneNumber(phoneNumberValue),
                Address.createAddress(addressBookValue)
        ));

        addressBook.remove(0);

        assertEquals(0, addressBook.size());
    }

    @Override
    @Test
    public void removeElementFromContainer_fail() {
        User user = new User(username);
        AddressBook addressBook = new AddressBook(user);

        addressBook.add(new Note(
                PhoneNumber.createPhoneNumber(phoneNumberValue),
                Address.createAddress(addressBookValue)
        ));

        addressBook.remove(0);

        assertThrows(IndexOutOfBoundsException.class,
                () -> addressBook.remove(0));
    }

    @Override
    @Test
    public void getElementFromContainer_success() {
        User user = new User(username);
        AddressBook addressBook = new AddressBook(user);
        Note note = new Note(
                PhoneNumber.createPhoneNumber(phoneNumberValue),
                Address.createAddress(addressBookValue)
        );

        addressBook.add(note);

        assertEquals(note, addressBook.get(0));
    }

    @Override
    @Test
    public void getElementFromContainer_fail() {
        User user = new User(username);
        AddressBook addressBook = new AddressBook(user);

        assertThrows(IndexOutOfBoundsException.class,
                () -> addressBook.get(0)
        );
    }
}
