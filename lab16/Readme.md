# Executing result
Executing the program and tests:
> `mvn clean package`
> 
> `java -jar target/lab16-1.0-SNAPSHOT.jar`
> 
result of executing first command:
```bash
[INFO] Scanning for projects...
[WARNING] 
[WARNING] Some problems were encountered while building the effective model for ua.khpi.oop.labs.yegorchevardin.lab16:lab16:jar:1.0-SNAPSHOT
[WARNING] 'build.plugins.plugin.version' for org.apache.maven.plugins:maven-compiler-plugin is missing. @ line 10, column 15
[WARNING] 
[WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
[WARNING] 
[WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
[WARNING] 
[INFO] 
[INFO] ------------< ua.khpi.oop.labs.yegorchevardin.lab16:lab16 >-------------
[INFO] Building lab13 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ lab16 ---
[INFO] Deleting /Users/yegorchevardin/Documents/university/2cond course/oop/labs/lab16/target
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ lab16 ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ lab16 ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 12 source files to /Users/yegorchevardin/Documents/university/2cond course/oop/labs/lab16/target/classes
[WARNING] /Users/yegorchevardin/Documents/university/2cond course/oop/labs/lab16/src/main/java/ua/khpi/oop/labs/yegorchevardin/lab16/Program.java: /Users/yegorchevardin/Documents/university/2cond course/oop/labs/lab16/src/main/java/ua/khpi/oop/labs/yegorchevardin/lab16/Program.java uses or overrides a deprecated API.
[WARNING] /Users/yegorchevardin/Documents/university/2cond course/oop/labs/lab16/src/main/java/ua/khpi/oop/labs/yegorchevardin/lab16/Program.java: Recompile with -Xlint:deprecation for details.
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ lab16 ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ lab16 ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 6 source files to /Users/yegorchevardin/Documents/university/2cond course/oop/labs/lab16/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ lab16 ---
[INFO] Surefire report directory: /Users/yegorchevardin/Documents/university/2cond course/oop/labs/lab16/target/surefire-reports
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-junit3/2.12.4/surefire-junit3-2.12.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-junit3/2.12.4/surefire-junit3-2.12.4.pom (1.7 kB at 3.7 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-providers/2.12.4/surefire-providers-2.12.4.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-providers/2.12.4/surefire-providers-2.12.4.pom (2.3 kB at 28 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-junit3/2.12.4/surefire-junit3-2.12.4.jar
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-junit3/2.12.4/surefire-junit3-2.12.4.jar (26 kB at 259 kB/s)

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running models.tools.AddressBookFinderTest
Tests run: 0, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.001 sec
Running models.classes.PhoneNumberTest
Tests run: 0, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.003 sec
Running models.classes.AddressTest
Tests run: 0, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0 sec
Running models.containers.AddressBookTest
Tests run: 0, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0 sec

Results :

Tests run: 0, Failures: 0, Errors: 0, Skipped: 0

[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ lab16 ---
[INFO] Building jar: /Users/yegorchevardin/Documents/university/2cond course/oop/labs/lab16/target/lab16-1.0-SNAPSHOT.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  7.817 s
[INFO] Finished at: 2023-05-22T17:22:02+02:00
[INFO] ------------------------------------------------------------------------

Process finished with exit code 0
```
result of executing second command:
```bash
Type here amount of notes you want to create
1
Please, type here address to add to element number: 1
st. Pushkinskaya KHarkiv
Please, type here phone number for the element number: 1
+380999425174
Please, register new Account: 
Please type your name: 
Yehor
Seeding passed elements to your user storage...
Container data: 
Length: 1
Elements: [Yehor][[[+380999425174], [st. Pushkinskaya KHarkiv]]]
---------Starting multithreading part---------
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
44 => pool-1-thread-1 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
44 => pool-1-thread-1
48 => pool-1-thread-5 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
48 => pool-1-thread-5
47 => pool-1-thread-4 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
47 => pool-1-thread-4
45 => pool-1-thread-2 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
45 => pool-1-thread-2
46 => pool-1-thread-3 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
46 => pool-1-thread-3
45 => pool-1-thread-2 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
45 => pool-1-thread-2
47 => pool-1-thread-4 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
47 => pool-1-thread-4
48 => pool-1-thread-5 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
48 => pool-1-thread-5
47 => pool-1-thread-4 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
47 => pool-1-thread-4
46 => pool-1-thread-3 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
46 => pool-1-thread-3
48 => pool-1-thread-5 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
48 => pool-1-thread-5
46 => pool-1-thread-3 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
46 => pool-1-thread-3
48 => pool-1-thread-5 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
48 => pool-1-thread-5
48 => pool-1-thread-5 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
48 => pool-1-thread-5
50 => pool-2-thread-2 started
51 => pool-2-thread-3 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
52 => pool-2-thread-4 started
53 => pool-2-thread-5 started

[]
[]
51 => pool-2-thread-3
49 => pool-2-thread-1 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
50 => pool-2-thread-2
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
49 => pool-2-thread-1
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
53 => pool-2-thread-5

[]
[]
52 => pool-2-thread-4
51 => pool-2-thread-3 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
51 => pool-2-thread-3
49 => pool-2-thread-1 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
52 => pool-2-thread-4 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
53 => pool-2-thread-5 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
49 => pool-2-thread-1

[]
[]
52 => pool-2-thread-4

[]
[]
53 => pool-2-thread-5
51 => pool-2-thread-3 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
51 => pool-2-thread-3
49 => pool-2-thread-1 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone
53 => pool-2-thread-5 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
49 => pool-2-thread-1

[]
[]
53 => pool-2-thread-5
51 => pool-2-thread-3 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
51 => pool-2-thread-3
49 => pool-2-thread-1 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
49 => pool-2-thread-1
49 => pool-2-thread-1 started
 finding all phone numbers in Kharkiv with Lifecell and Vodafone

[]
[]
49 => pool-2-thread-1
           Results           
    Type name      |  Time  |
-----------------------------
     Synchronized  |   171  |
 Non synchronized  |   208  |
           Result  |    37  |
---------Testing serialization part---------
Storing address book in file...
null

Process finished with exit code 0

```