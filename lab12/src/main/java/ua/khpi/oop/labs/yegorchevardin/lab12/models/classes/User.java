package ua.khpi.oop.labs.yegorchevardin.lab12.models.classes;

import java.io.Serializable;
import java.util.Date;

/**
 * User representation as DTO object
 */
public class User implements Serializable {
    private String name;

    public User(String name) {
        this.name = name;
    }

    public User() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "[" + name + "]";
    }
}
