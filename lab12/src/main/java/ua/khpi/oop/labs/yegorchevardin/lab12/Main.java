package ua.khpi.oop.labs.yegorchevardin.lab12;

/**
 * 12 laboratory work
 * Add text finding from address book using regex.
 *
 * @version 28 Mar 2023
 * @author Yegor Chevardin
 */
public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		Program.getInstance().start();
	}
}
