package ua.khpi.oop.labs.yegorchevardin.lab12;

import ua.khpi.oop.labs.yegorchevardin.lab12.exceptions.DataInvalidException;
import ua.khpi.oop.labs.yegorchevardin.lab12.models.classes.Address;
import ua.khpi.oop.labs.yegorchevardin.lab12.models.classes.Note;
import ua.khpi.oop.labs.yegorchevardin.lab12.models.classes.PhoneNumber;
import ua.khpi.oop.labs.yegorchevardin.lab12.models.classes.User;
import ua.khpi.oop.labs.yegorchevardin.lab12.models.containers.AddressBook;
import ua.khpi.oop.labs.yegorchevardin.lab12.models.containers.UserStorage;
import ua.khpi.oop.labs.yegorchevardin.lab12.tools.AddressBookFinder;

import java.sql.Date;
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

/**
 * Class that represents prgoram
 */
public class Program {
    private static Program instance = null;

    private Program() {}

    /**
     * Gets only one instance of the program
     */
    public static Program getInstance() {
        if (instance == null) {
            instance = new Program();
        }
        return instance;
    }

    /**
     * Starts the program
     */
    public void start() {
        AddressBook addressBook = new AddressBook();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Type here amount of notes you want to create");
        int times = 1;
        try {
            times = getIntegerFromInput(scanner);
        } catch (IllegalArgumentException e) {
            System.out.println("Bounds must be positive number!");
            System.exit(0);
        }
        Note[] seedingNotes = new Note[times];
        for (int i = 0; i < seedingNotes.length; i++) {
            System.out.println("Please, type here address to add to element number: " + (i + 1));
            String address = scanner.nextLine();
            System.out.println("Please, type here phone number for the element number: " + (i + 1));
            String phoneNumber = scanner.nextLine();
            try {
                seedingNotes[i] = new Note(
                        PhoneNumber.createPhoneNumber(phoneNumber),
                        Address.createAddress(address)
                );
            } catch (DataInvalidException e) {
                System.out.println("Data you input is invalid!");
                System.out.println(e.getMessage());
                System.exit(0);
            }
        }
        run(scanner, addressBook, seedingNotes);
    }

    private <T> void run(Scanner scanner, UserStorage<T> userStorage, T[] elements) {
        try {
            System.out.println("Please, register new Account: ");
            User user = register(scanner);
            userStorage.setUser(user);
            System.out.println("Seeding passed elements to your user storage...");
            userStorage.addAll(Arrays.asList(elements));
            printContainer(userStorage);

            AddressBook addressBook = (AddressBook) userStorage;
            System.out.println("Finding all phone numbers in kharkiv with Kyivstar:");
            System.out.println(
                    AddressBookFinder
                            .findAllNotesByKyivstarNumbersInKharkiv(addressBook)
            );
            System.out.println("Finding all phone numbers in Kharkiv with Lifecell");
            System.out.println(
                    AddressBookFinder
                            .findAllNotesByLifesellNumberInKharkiv(addressBook)
            );
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
    }

    private static User register(Scanner scanner) {
        System.out.println("Please type your name: ");
        String name = scanner.nextLine();
        return new User(name);
    }

    private <T> void printContainer(UserStorage<T> container) {
        System.out.println("Container data: ");
        System.out.println("Length: " + container.size());
        System.out.println("Elements: " + container);
    }

    private static int getIntegerFromInput(Scanner scanner) {
        try {
            return Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You typed wrong value, please, try again: ");
            return getIntegerFromInput(scanner);
        }
    }
}
