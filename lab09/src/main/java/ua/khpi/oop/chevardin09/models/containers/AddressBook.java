package ua.khpi.oop.chevardin09.models.containers;

import jakarta.xml.bind.annotation.XmlRootElement;
import ua.khpi.oop.chevardin09.models.classes.Note;
import ua.khpi.oop.chevardin09.models.classes.User;


/**
 * Address book that represents container and extends from UserStorage
 */
public class AddressBook extends UserStorage<Note> {
    public AddressBook(User user) {
        super(user);
    }

    public AddressBook() {}
}
