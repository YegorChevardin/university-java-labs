package ua.khpi.oop.chevardin09;

import jakarta.xml.bind.*;
import ua.khpi.oop.chevardin09.models.classes.Note;
import ua.khpi.oop.chevardin09.models.containers.AddressBook;
import ua.khpi.oop.chevardin09.models.classes.User;
import ua.khpi.oop.chevardin09.models.classes.Address;
import ua.khpi.oop.chevardin09.models.classes.PhoneNumber;

import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.Date;
import java.util.Scanner;

public class AddressBookProgram {
    private static final String PATH_TO_FILE = "./example.ser";
    private static final String PATH_TO_XML_FILE = "./example.xml";
    private static AddressBookProgram instance = null;
    private User user;

    private AddressBook addressBook;

    private AddressBookProgram() {}

    public static AddressBookProgram getInstance() {
        if (instance == null) {
            instance = new AddressBookProgram();
        }

        return instance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AddressBook getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(AddressBook addressBook) {
        this.addressBook = addressBook;
    }

    /**
     * Method that starts the program
     */
    public void start() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("-------------------");
        System.out.println("Please, register new account: ");
        user = register(scanner);

        System.out.println("-------------------");
        System.out.println("Creating address book...");
        addressBook = new AddressBook(user);
        seedAddressBook(addressBook, scanner);
        System.out.println("Size of the book: " + addressBook.size());
        System.out.println(addressBook);
        serializeAddressBook(addressBook);
        Object container = deserializeAddressBook();
        System.out.println("After deserialization address book looks like:");
        System.out.println(container);
        storeAddressBook(addressBook);
        AddressBook storedAddressBook = getFromStorageAddressBook();
        System.out.println("After getting address book from the file without serializing algorithm it looks like: ");
        System.out.println(storedAddressBook);
        System.out.println("Deleting first element from the address book...");
        addressBook.remove(addressBook.size() - 1);
        System.out.println("Size of the address book now is: " + addressBook.size());
        System.out.println(addressBook);
    }

    private static void seedAddressBook(AddressBook addressBook, Scanner scanner) {
        System.out.println("-------------------");
        System.out.println("Seeding address book...");
        System.out.println("Please, type number of notes you want to create: ");
        int numberOfNotes = inputIntNumber(scanner);
        System.out.println("Typing information for notes");
        for (int i = 1; i <= numberOfNotes; i++) {
            System.out.println("Typing information for note " + i + ":");
            System.out.println("Please, type address:");
            String address = scanner.nextLine();
            System.out.println("Please, type phone number:");
            String phoneNumber = scanner.nextLine();
            System.out.println("Creating note...");
            try {
                addressBook.add(
                        new Note(
                                new PhoneNumber(phoneNumber),
                                new Address(address)
                        )
                );
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage() + ": " + phoneNumber);
                //if error exiting the program
                System.exit(0);
            }
            System.out.println("----");
        }
    }

    private static User register(Scanner scanner) {
        String name, fatherName, secondName;
        int day, month, year;

        System.out.println("Please type your name: ");
        name = scanner.nextLine();
        System.out.println("Please type your second name: ");
        secondName = scanner.nextLine();
        System.out.println("Please type your father name: ");
        fatherName = scanner.nextLine();
        System.out.println("Please type a day of birth: ");
        day = inputIntNumber(scanner);
        System.out.println("Please type a month of birth: ");
        month = inputIntNumber(scanner);
        System.out.println("Please type a year of birth: ");
        year = inputIntNumber(scanner);
        Date birthDay = new Date(year - 1900, month - 1, day);

        return new User(name, secondName, fatherName, birthDay);
    }

    /**
     * Method that takes integer from input
     * @param scanner Scenner that will be used while taking a number from input
     * @return integer that was typed by user
     */
    private static int inputIntNumber(Scanner scanner) {
        int number = 0;
        try {
            number = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Wrong number typed, 0 was returned!");
        }
        return number;
    }

    private static void serializeAddressBook(AddressBook addressBook) {
        System.out.println("Storing address book in file...");
        try (FileOutputStream fileOutputStream = new FileOutputStream(PATH_TO_FILE);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(addressBook);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Storing completed successfully...");
    }

    private static Object deserializeAddressBook() {
        System.out.println("Deserializing the object...");
        Object container;
        try (FileInputStream fileInputStream = new FileInputStream(PATH_TO_FILE);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            container = objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Deserializing object completed...");
        return container;
    }

    private static AddressBook getFromStorageAddressBook() {
        File xmlFile = new File(PATH_TO_XML_FILE);
        JAXBContext jaxbContext;

        try {
            jaxbContext = JAXBContext.newInstance(AddressBook.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            JAXBElement<AddressBook> jaxbElement = jaxbUnmarshaller
                    .unmarshal(new StreamSource(xmlFile), AddressBook.class);
            return jaxbElement.getValue();
        }
        catch (JAXBException e)
        {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static void storeAddressBook(AddressBook addressBook) {
        try {
            JAXBElement<AddressBook> jaxbElement = new JAXBElement<>(new QName("", "AddressBook"), AddressBook.class, addressBook);
            JAXBContext jaxbContext = JAXBContext.newInstance(AddressBook.class);

            //Create Marshaller
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Store XML to File
            File file = new File(PATH_TO_XML_FILE);

            //Writes XML file to file-system
            jaxbMarshaller.marshal(jaxbElement, file);
        } catch (JAXBException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
