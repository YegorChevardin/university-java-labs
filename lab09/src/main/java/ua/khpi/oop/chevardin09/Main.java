package ua.khpi.oop.chevardin09;

/**
 * 9 laboratory work
 * Create a generic container based on domain objects of laboratory work number 7.
 *
 * @version 11 Mar 2023
 * @author Yegor Chevardin
 */
public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		AddressBookProgram program = AddressBookProgram.getInstance();
		program.start();
	}
}
