package ua.khpi.oop.chevardin09.models.classes;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Phone number represent serializable phone number as DTO object
 */
public class PhoneNumber implements Serializable {
    private static final String PHONE_REGEX = "^[\\+]?[(]?[0-9]{3}[)]?[-\\s\\.]?[0-9]{3}[-\\s\\.]?[0-9]{4,6}$";
    private final String value;

    public PhoneNumber(String value) {
        if (!validatePhoneNUmber(value)) {
            throw new IllegalArgumentException("Inserted values is not phone number!");
        }
        this.value = value;
    }

    /**
     * Method that validates string whether it is phone number or not
     * @param phoneNumber string to be validated
     * @return valid or not (true or false)
     */
    public static boolean validatePhoneNUmber(String phoneNumber) {
        Pattern pattern = Pattern.compile(PHONE_REGEX);
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.matches();
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "[" + value + "]";
    }
}
