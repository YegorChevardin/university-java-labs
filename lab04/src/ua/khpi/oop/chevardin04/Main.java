package ua.khpi.oop.chevardin04;

import java.util.Scanner;

/**
 * 3 laboratory work
 * Task 3. Enter text. Replace specific character from text without using replaceAll() and other methods of this kind and build UI for it
 *
 * @version 1 26 Sep 2022
 * @author Yegor Chevardin
 */

public class Main {
	/**
	 * The point of enter
	 * @param args
	 */
	public static void main(String[] args) {
		UiInterface uiInterface = new UiInterface();
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-h") || args[i].equals("-help")) {
				uiInterface.help();
			} else if (args[i].equals("-d") || args[i].equals("-debug")) {
				uiInterface.setDebug(true);
				System.out.println("You turned on debug mode.");
			}
		}
		uiInterface.start();
	}
}
