package ua.khpi.oop.chevardin04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ReplaceText {
    /**
     * constructor for utility class
     */
    private ReplaceText() {}

    /**
     * method that cuts string into the words, using scanner class
     * @param text Text, that will be split
     * @return list of words
     */
    private static List<String> cutString(String text) {
        List<String> stringList = new ArrayList<>();

        Scanner scanner = new Scanner(text);
        while (scanner.hasNext()) {
            stringList.add(scanner.next());
        }

        return stringList;
    }

    /**
     * method that replaces selected character at selected position in the word
     * @param character selected character
     * @param place selected place in the text
     * @param  word word, which characters will be replaced
     * @param  information shows detailed information about application thread
     * @return new word with replaced characters
     * */
    private static String replaceCharacter(String word, int place, char character, boolean information) {
        char[] wordCharacters = word.toCharArray();

        if (information) System.out.println("Entered word as an array of chars: " + Arrays.toString(wordCharacters));

        if (!(place > wordCharacters.length)) {
            for (int i = 0; i < wordCharacters.length; i++) {
                if (i == place - 1) {
                    if (information) System.out.println("Current char" + wordCharacters[i] + " will be replaced with this: " + character);
                    wordCharacters[i] = character;
                }
            }
        } else {
            System.out.println("Cannot replace this word!");
        }
        return String.valueOf(wordCharacters);
    }

    /**
     * method that replaces selected character at selected position in the text
     * @param newCharacter selected character
     * @param k selected place in the text
     * @param  text text, which characters will be replaced
     * */
    public static void replaceString(String text, int k, char newCharacter, boolean information) {
        List<String> words = cutString(text);
        if (information) System.out.println("Entered text as a Array of words: " + Arrays.toString(words.toArray()));
        words.replaceAll(word -> replaceCharacter(word, k, newCharacter, information));
        StringBuilder stringBuilder = new StringBuilder();
        words.forEach(s -> {
            if (words.indexOf(s) == words.size() - 1) {
                stringBuilder.append(s);
            } else {
                stringBuilder.append(s).append(" ");
            }
        });
        String newText = stringBuilder.toString();

        print(text, newText);
    }

    /**
     * prints old text and new text into console and old and new words
     * @param text old text
     * @param newText new text
     */
    private static void print(String text, String newText) {
        List<String> words = cutString(text);
        List<String> newWords = cutString(newText);
        printTexts(text, newText);
        System.out.println("--------------------------------");
        printTable(words, newWords);
    }

    /**
     * prints old text and new text into console
     * @param text old text
     * @param newText new text
     */
    private static void printTexts(String text, String newText) {
        System.out.println("Old text: " + text);
        System.out.println("New text: " + newText);
    }

    /**
     * prints table of the replaced words
     * @param words old words
     * @param newWords new text
     */
    private static void printTable(List<String> words, List<String> newWords) {
        if (words.size() != newWords.size()) throw new IllegalArgumentException("This words are not from the same text!");
        System.out.println("Old word   New word");
        for (int i = 0; i < words.size(); i++) {
            System.out.println(words.get(i) + "   " + newWords.get(i));
        }
    }
}
