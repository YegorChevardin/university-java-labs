package ua.khpi.oop.chevardin04;

import java.util.Scanner;

public class UiInterface {
    private boolean debug = false;
    private boolean settedValues = false;
    private String text;
    private int number;
    private char character;

    /**
     * method to run the program with selected values
     * @param text text that will be chabged
     * @param number number of character position in word that will be chabged
     * @param character new value for old character
     */
    private void run(String text, int number, char character) {
        ReplaceText.replaceString(text, number, character, debug);
    }

    /**
     * method to set values for application running
     * */
    private void setValues() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, type here your text (in line): ");
        text = scanner.nextLine();
        System.out.println("Please, type here number of character to replace (in line): ");
        number = 0;
        try {
            number = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Wrong number typed, 0 selected as a replace number!");
        }
        System.out.println("Please, type here character to replace (in line, if string inputted - first character of that string will be selected): ");
        String characterString = scanner.nextLine();
        character = characterString.charAt(0);
        settedValues = true;
    }

    /**
     * method to dispaly values of entered values
     * */
    private void displayValues() {
        System.out.println("Entered text: " + text);
        System.out.println("Entered number: " + number);
        System.out.println("Entered character: " + character);
    }

    /**
     * method to print help information
     * */
    public void help() {

    }

    /**
     * matches entered values if its validate for application
     * @param values values that need to match
     * */
    private boolean matchOptions(String values) {
        boolean result = false;
        switch (values) {
            case "1":
                setValues();
                break;
            case "2":
                if (settedValues) {
                    displayValues();
                } else {
                    System.out.println("Walues are not entered yet!");
                }
                break;
            case "3":
                if (settedValues) {
                    result = true;
                } else {
                    System.out.println("The values are not selected!");
                }
                break;
            case "4":
                System.exit(0);
                break;
            default:
                System.out.println("You printed wrong value!");
        }
        return result;
    }

    /**
     * method that starts main application thread
     * */
    public void start() {
        System.out.println();
        System.out.println("Words changing");
        System.out.println("[Enter a number to choose an action]");
        System.out.println("1 - Set values");
        System.out.println("2 - Display values");
        System.out.println("3 - Run program");
        System.out.println("4 - Exit");
        System.out.println();
        String value = new Scanner(System.in).nextLine();
        if (matchOptions(value)) {
            run(text, number, character);
        } else {
            start();
        }
    }

    /**
     * method that sets debug mode on
     * @param debug debug mode value
     * */
    public void setDebug(boolean debug) {
        this.debug = debug;
    }
}
