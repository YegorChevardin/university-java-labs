package ua.khpi.oop.chevardin06;

import ua.khpi.oop.chevardin06.containers.InformationContainer;

import java.io.*;

/**
 * 6 laboratory work
 * Make an example of serialization and deserialization of container from lab 5, by adding new methods to them (compare, sort and find)
 *
 * @version 1 2 Nov 2022
 * @author Yegor Chevardin
 */
public class Main {
	private static final String PATH_TO_FILE = "./example.ser";

	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		ApplicationStart application = ApplicationStart.getInstance();
		application.start();

		InformationContainer currentContainer = application.getContainer();
		storeContainerToFile(currentContainer, PATH_TO_FILE);

		Object containerFromFile = getContainerFromFile(PATH_TO_FILE);

		System.out.println("The result of serializing and comparing object by compareContainer() method: " + currentContainer.compareContainer((InformationContainer) containerFromFile)
		+ " and by default equals() method: " + currentContainer.equals(containerFromFile)
		+ ". Hash code of this object (equals or not): " + (currentContainer.hashCode() == containerFromFile.hashCode()));

		System.out.println("All objects from serialized object: " + containerFromFile.toString() + " and size: " + ((InformationContainer) containerFromFile).size()
		+ " and for example last element: " + ((InformationContainer) containerFromFile).get(((InformationContainer) containerFromFile).size() - 1));
	}

	/**
	 * Method to store an object in file
	 */
	private static void storeContainerToFile(InformationContainer container, final String path) {
		try (FileOutputStream fout = new FileOutputStream(path);
			 ObjectOutputStream oos = new ObjectOutputStream(fout)) {
			oos.writeObject(container);
		} catch (IOException e) {
			System.out.println("An error occur while writing to file: " + e.getMessage());
		}
	}

	/**
	* Method to deserialize object
	**/
	private static Object getContainerFromFile(final String path) {
		Object container = null;
		try (FileInputStream fin = new FileInputStream(path);
			 ObjectInputStream oos = new ObjectInputStream(fin)) {
			container = oos.readObject();
		} catch (IOException e) {
			System.out.println("An error occur while writing to file: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println("An error while getting class object: " + e.getMessage());
		} catch (ClassCastException e) {
			System.out.println("An error while casting class object: " + e.getMessage());
		}
		return container;
	}
}
