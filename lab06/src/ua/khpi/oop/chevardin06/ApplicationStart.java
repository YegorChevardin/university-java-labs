package ua.khpi.oop.chevardin06;

import ua.khpi.oop.chevardin06.containers.InformationContainer;
import ua.khpi.oop.chevardin06.util.ReplaceText;

import java.util.Scanner;

public class ApplicationStart {
    private static ApplicationStart instance = null;
    private InformationContainer container = new InformationContainer();

    private ApplicationStart() {}

    /**
     * Method that returns ApplicationStart instance
     * @return instance of current class
     */
    public static ApplicationStart getInstance() {
        if (instance == null) {
            instance = new ApplicationStart();
        }
        return instance;
    }

    /**
     * Method that starts application logic
     */
    public void start() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, type here how much words do you want to change: ");
        final int count = inputIntNumber(scanner);

        for (int i = 0; i < count; i++) {
            System.out.println("Please, type here your word (in line): ");
            String text = scanner.nextLine();
            System.out.println("Please, type here character number to replace (in line): ");
            int number = inputIntNumber(scanner);
            System.out.println("Please, type here character to replace (in line, if string inputted - first character of that string will be selected): ");
            String characterString = scanner.nextLine();
            char character = characterString.charAt(0);
            container.add(ReplaceText.replaceString(text, number, character));
        }

        System.out.println("All words in this program: " + container + ". Size of the container is: " + container.size());
    }

    /**
     * Method that takes integer from input
     * @param scanner Scenner that will be used while taking a number from input
     * @return integer that was typed by user
     */
    private static int inputIntNumber(Scanner scanner) {
        int number = 0;
        try {
            number = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Wrong number typed, 0 was returned!");
        }
        return number;
    }

    public InformationContainer getContainer() {
        return container;
    }
}
