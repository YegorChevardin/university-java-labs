package ua.khpi.oop.chevardin01;

public class Main {

    private static final int DECENT_NUMBER = 0x006F; // 22 => 006F
    private static final long TELEPHONE_NUMBER = 48574350682L;
    private static final int LAST_TWO_NUMBERS_FROM_PHONE = 0b1010010; // 82 => 001010010
    private static final int LAST_FOUR_NUMBERS_FROM_PHONE = 13062; // 5682 => 13062
    private static final int SECTION_NUMBER_FIVE = ((22 - 1) % 26) + 1;
    private static final char LETTER = 'V';

    /**
     * Method that counts odd numbers from a given bunch of numbers
     * @return number of odds
     * @param number bunch of numbers from which odd numbers will be counted */
    static int oddCount(int... number) {
        int oddCount = 0;
        for (int j : number) if (j % 2 == 0) oddCount++;
        return oddCount;
    }

    /**
     * Method that counts the number of 1 from a given bunch of numbers
     * @return number of 1
     * @param number bunch of numbers from which 1 will be counted */
    static int onesCount(int... number) {
        int onesCount = 0;
        for (int i = 0; i < number.length; i++) {
            while (number[i] != 0) {
                if (number[i] % 2 != 0) {
                    onesCount++;
                }
                number[i] /= 2;
            }
        }
        return onesCount;
    }

    public static void main(String[] args) {
        int result = oddCount(DECENT_NUMBER, (int) TELEPHONE_NUMBER, LAST_TWO_NUMBERS_FROM_PHONE, LAST_FOUR_NUMBERS_FROM_PHONE);
        int result1 = onesCount(DECENT_NUMBER, (int) TELEPHONE_NUMBER, LAST_TWO_NUMBERS_FROM_PHONE, LAST_FOUR_NUMBERS_FROM_PHONE);
    }
}